<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);

$number_of_student = count(file("dssv.csv"));
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Danh sách sinh viên</title>
</head>
<body>
    <form class="home">
        <div class="home-input-field-div">
            <div class="label-div-without-border-n-bg">
                <label for="faculty">Khoa</label>
            </div>
            <select name="faculty" id="faculty">
				<?php
					echo "<option></option>";
					foreach ($faculties as $faculty) {
						echo "<option value=\"$faculty\">$faculty</option>";
					}
				?>
			</select>
        </div>
        <div class="home-input-field-div">
            <div class="label-div-without-border-n-bg">
                <label for="keyword">Từ khoá</label>
            </div>
            <input class="home-text-input" type="text" id="keyword" name="keyword">
        </div>
        <div class="submit-div">
            <input type="submit" value="Tìm kiếm"></input>
        </div>
    </form>
    <div class="student-list">
        <div class="student-list-header">
            <div>
                <?php echo "<p>Số sinh viên tìm thấy: " . $number_of_student . "</p>"; ?>
            </div>
            <div>
                <input type="submit" value="Thêm" onclick="location.href='form.php'"/>
            </div>
        </div>

        <table>
            <tr>
                <td width="100">No</td>
                <td>Tên sinh viên</td>
                <td>Khoa</td>
                <td>Action</td>
            </tr>
            <?php
                $student_list = fopen("dssv.csv", "r");
                while (!feof($student_list)) {
                    $student_info = fgets($student_list);
                    $splitted = explode(",", $student_info);
                    echo "<tr>";
                    echo "<td>$splitted[0]</td>";
                    echo "<td>$splitted[1]</td>";
                    echo "<td>$splitted[2]</td>";
                    echo "<td><button>Xoá</button>  <button>Sửa</button></td>";
                    echo "</tr>";
                }
                fclose($student_list);
            ?>
        </table>
    </div>
</body>
</html>